﻿using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        private readonly IGraphService _graphService;

        public IMvxCommand GoBackCommand => new MvxCommand(() => Close(this));

        public BaseViewModel(IGraphService graphService)
        {
            _graphService = graphService;
        }

        public virtual void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        public IMvxCommand LogoutCommand => new MvxCommand(Logout);
        private void Logout()
        {
            _graphService.RemoveUser();
            ChangePresentation(new LoginPresentationHint());
        }
    }
}
