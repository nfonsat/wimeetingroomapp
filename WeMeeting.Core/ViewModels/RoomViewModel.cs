﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class RoomViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;
        private readonly IConfigService _configService;

        private string _qRCode = string.Empty;
        public string QRCode
        {
            get => _qRCode;
            set => SetProperty(ref _qRCode, value);
        }

        private MvxObservableCollection<HeaderListModel<ScheduleItemModel>> _scheduleHeaderList;
        public MvxObservableCollection<HeaderListModel<ScheduleItemModel>> ScheduleHeaderList
        {
            get => _scheduleHeaderList;
            set => SetProperty(ref _scheduleHeaderList, value);
        }

        private MvxObservableCollection<ScheduleItemModel> _schedule;
        public MvxObservableCollection<ScheduleItemModel> Schedule
        {
            get => _schedule;
            set
            {
                SetProperty(ref _schedule, value);

                ScheduleHeaderList = new MvxObservableCollection<HeaderListModel<ScheduleItemModel>>();

                foreach (ScheduleItemModel schedule in value)
                {
                    string h = DateTime.Parse(schedule.Start.DateTime).ToLocalTime().ToString("D");
                    HeaderListModel<ScheduleItemModel> header = null;

                    for (int i = 0; i < ScheduleHeaderList.Count; ++i)
                    {
                        if (ScheduleHeaderList[i].Title == h)
                        {
                            header = ScheduleHeaderList[i];
                            break;
                        }
                    }

                    if (header == null)
                    {
                        header = new HeaderListModel<ScheduleItemModel>(h);
                        ScheduleHeaderList.Add(header);
                    }

                    header.Add(schedule);
                }
            }
        }

        public RoomViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _configService = configService;
            _graphService = graphService;
        }

        public IMvxCommand ScanQrCodeCommand => new MvxAsyncCommand(ScanQrCodeAsync);
        private async Task ScanQrCodeAsync()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();

            var result = await scanner.Scan();

            InvokeOnMainThread(() => QRCode = result.Text);

            var now = DateTime.Now;
            var end = now.AddDays(7);

            var scheduleQuery = new ScheduleQueryModel
            {
                StartTime = DateTimeModel.CreateModel(now, TimeZoneInfo.Local),
                EndTime = DateTimeModel.CreateModel(end, TimeZoneInfo.Local),
                Schedules = new string[] { QRCode },
                AvailabilityViewInterval = "15"
            };

            var list = await _graphService.GetFreeBusyCalendarAsync(scheduleQuery);
            InvokeOnMainThread(() => Schedule = new MvxObservableCollection<ScheduleItemModel>(list.Value[0].ScheduleItems));
        }
    }
}
