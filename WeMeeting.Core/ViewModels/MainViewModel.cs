﻿using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IConfigService _configService;
        private readonly IGraphService _graphService;

        private UserModel _user = null;
        public UserModel User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public MainViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
            User = _configService.User;
        }

        public IMvxCommand ShowCalendarCommand => new MvxCommand(ShowCalendar);
        private void ShowCalendar()
        {
            ShowViewModel<ViewModels.CalendarViewModel>();
        }

        public IMvxCommand BookingRoomCommand => new MvxCommand(BookingRoom);
        private void BookingRoom()
        {
            ShowViewModel<ViewModels.BookViewModel>();
        }

        public IMvxCommand ShowRoomCommand => new MvxCommand(ShowRoom);
        private void ShowRoom()
        {
            ShowViewModel<ViewModels.RoomViewModel>();
        }
    }
}
