﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class BookRoomViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;
        private readonly IConfigService _configService;

        private EmailAddressModel _room;
        public EmailAddressModel Room
        {
            get => _room;
            set => SetProperty(ref _room, value);
        }

        private string _bookSubject;
        public string BookSubject
        {
            get => _bookSubject;
            set => SetProperty(ref _bookSubject, value);
        }

        private List<UserModel> _userSuggestions;
        public List<UserModel> UserSuggestions
        {
            get
            {
                if (_userSuggestions == null)
                {
                    _userSuggestions = new List<UserModel>();
                }
                return _userSuggestions;
            }
            set => SetProperty(ref _userSuggestions, value);
        }

        private MvxObservableCollection<EntityWrap<UserModel>> _bookAttendees;
        public MvxObservableCollection<EntityWrap<UserModel>> BookAttendees
        {
            get => _bookAttendees;
            set => SetProperty(ref _bookAttendees, value);
        }

        private DateTime _bookFrom;
        public DateTime BookFrom
        {
            get => _bookFrom;
            set => SetProperty(ref _bookFrom, value);
        }

        private DateTime _bookTo;
        public DateTime BookTo
        {
            get => _bookTo;
            set => SetProperty(ref _bookTo, value);
        }

        private string _currentTextHint;
        public string CurrentTextHint
        {
            get =>  _currentTextHint;
            set
            {
                //Setting _currentTextHint to null if an empty string gets passed here
                //is extremely important.
                if (value == "")
                {
                    _currentTextHint = null;
                    UserSuggestions = new List<UserModel>();
                    return;
                }
                else
                {
                    _currentTextHint = value;
                }

                if (_currentTextHint.Trim().Length < 2)
                {
                    UserSuggestions = new List<UserModel>();
                    return;
                }

                System.Threading.Tasks.Task.Run(async () =>
                {
                    var res = await _graphService.FindPeopleAsync(CurrentTextHint);

                    if (res.Length == 0)
                    {
                        UserSuggestions = new List<UserModel>();
                    }
                    else
                    {
                        UserSuggestions = new List<UserModel>();
                        UserSuggestions.AddRange(res);
                    }
                });
            }
        }

        private string _attendeeText;
        public string AttendeeText
        {
            get => _attendeeText;
            set => SetProperty(ref _attendeeText, value);
        }

        private UserModel _selectAttendee;
        public UserModel SelectAttendee
        {
            get => _selectAttendee;
            private set
            {
                BookAttendees.Add(new EntityWrap<UserModel>(value, DeleteAttendee));
                AttendeeText = "";
            }
        }

        public BookRoomViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        public override void ViewAppearing()
        {
            Room = _configService.EmailAddress;

            DateTime aux = DateTime.Now;
            BookFrom = new DateTime(aux.Year, aux.Month, aux.Day, aux.Hour, aux.Minute, 0);
            BookTo = BookFrom.AddMinutes(30);

            BookAttendees = new MvxObservableCollection<EntityWrap<UserModel>>();

            base.ViewAppearing();
        }

        public IMvxCommand ConfirmBookRoomCommand => new MvxCommand(ConfirmBookRoom);
        private void ConfirmBookRoom()
        {
            System.Threading.Tasks.Task.Run(async () =>
            {
                List<AttendeeModel> attendees = new List<AttendeeModel>();

                attendees.Add(new AttendeeModel { EmailAddress = Room });
                foreach (EntityWrap<UserModel> user in BookAttendees)
                {
                    attendees.Add(new AttendeeModel
                    {
                        EmailAddress = new EmailAddressModel
                        {
                            Name =  user.Entity.DisplayName,
                            Address = user.Entity.UserPrincipalName
                        }
                    });
                }

                CalendarModel calendar = new CalendarModel
                {
                    Start = DateTimeModel.CreateModel(BookFrom, TimeZoneInfo.Local),
                    End = DateTimeModel.CreateModel(BookTo, TimeZoneInfo.Local),
                    Subject = BookSubject,
                    Attendees = attendees.ToArray(),
                    Locations = new LocationModel[]
                    {
                        new LocationModel
                        {
                            DisplayName = Room.Name,
                            LocationType = "conferenceRoom"
                        }
                    }
                };

                var res = await _graphService.AddEventAsync(calendar);

                Close(this);
            });
        }

        private void DeleteAttendee(UserModel model)
        {
            int i = 0;

            for (; i < BookAttendees.Count; ++i)
            {
                if (BookAttendees[i].Entity == model)
                {
                    break;
                }
            }

            if (i < BookAttendees.Count)
                BookAttendees.RemoveAt(i);
        }

    }
}
