﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;

        private readonly IConfigService _configService;

        public LoginViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        public IMvxCommand LoginCommand => new MvxAsyncCommand(LoginAsync);
        private async Task LoginAsync()
        {
            try
            {

                // Make sure that the Graph service is configured.
                await _graphService.EnsureTokenIsPresentAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            UserModel user = null;

            // Get the current user, its groups and all of the groups.
            await Task.WhenAll(
                Task.Run(async () =>
                {
                    user = await _graphService.GetUserAsync();
                })
            );

            _configService.User = user;

            ShowViewModel<ViewModels.MainViewModel>();
        }
    }
}
