﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class BookViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;
        private readonly IConfigService _configService;

        private MvxObservableCollection<EmailAddressModel> _rooms;
        public MvxObservableCollection<EmailAddressModel> Rooms
        {
            get => _rooms;
            set => SetProperty(ref _rooms, value);
        }

        public BookViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        public override void ViewAppearing()
        {
            System.Threading.Tasks.Task.Run(async () =>
            {
                var list = await _graphService.GetRoomsAsync();

                InvokeOnMainThread(() => Rooms = new MvxObservableCollection<EmailAddressModel>(list));
            });

            base.ViewAppearing();
        }

        public IMvxCommand ScanQrCodeCommand => new MvxAsyncCommand(ScanQrCodeAsync);
        private async Task ScanQrCodeAsync()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan();

            EmailAddressModel room = new EmailAddressModel { Address = result.Text, Name = "" };
         
        }

        public IMvxCommand ShowBookRoomCommand => new MvxCommand<EmailAddressModel>(BookRoom);
        private void BookRoom(EmailAddressModel item)
        {
            _configService.EmailAddress = item;
            ShowViewModel<ViewModels.BookRoomViewModel>();
        }
    }
}
