﻿using System;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class CalendarViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;

        private readonly IConfigService _configService;

        private MvxObservableCollection<HeaderListModel<CalendarModel>> _calendarList;
        public MvxObservableCollection<HeaderListModel<CalendarModel>> CalendarList
        {
            get => _calendarList;
            set => SetProperty(ref _calendarList, value);
        }

        private MvxObservableCollection<CalendarModel> _calendars;
        public MvxObservableCollection<CalendarModel> Calendars
        {
            get => _calendars;
            set
            {
                SetProperty(ref _calendars, value);

                CalendarList = new MvxObservableCollection<HeaderListModel<CalendarModel>>();

                foreach (CalendarModel calendar in value)
                {
                    string h = DateTime.Parse(calendar.Start.DateTime).ToLocalTime().ToString("D");
                    HeaderListModel<CalendarModel> header = null;

                    for (int i = 0; i < CalendarList.Count; ++i)
                    {
                        if (CalendarList[i].Title == h)
                        {
                            header = CalendarList[i];
                            break;
                        }
                    }

                    if (header == null)
                    {
                        header = new HeaderListModel<CalendarModel>(h);
                        CalendarList.Add(header);
                    }

                    header.Add(calendar);
                }
            }
        }

        public CalendarViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        public override void ViewAppearing()
        {
            var now = DateTime.Today;
            var end = now.AddDays(7);

            System.Threading.Tasks.Task.Run(async () =>
            {
                var list = await _graphService.GetCalendarMeAsync(now, end);
                InvokeOnMainThread(() =>  Calendars = new MvxObservableCollection<CalendarModel>(list));
            });

            base.ViewAppearing();
        }

        public IMvxCommand ShowBookDetailsCommand => new MvxCommand<CalendarModel>(RoomDetails);
        private void RoomDetails(CalendarModel item)
        {
            _configService.Calendar = item;
            ShowViewModel<ViewModels.BookDetailsViewModel>();
        }
    }
}
