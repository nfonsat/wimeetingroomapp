﻿using System;
using MvvmCross.Core.ViewModels;
using WeMeeting.Core.Models;
using WeMeeting.Core.Services;

namespace WeMeeting.Core.ViewModels
{
    public class BookDetailsViewModel : BaseViewModel
    {
        private readonly IGraphService _graphService;
        private readonly IConfigService _configService;

        private CalendarModel _bookingDetails;
        public CalendarModel BookingDetails
        {
            get => _bookingDetails;
            set => SetProperty(ref _bookingDetails, value);
        }

        private DateTime _bookingFrom;
        public DateTime BookingFrom
        {
            get => _bookingFrom;
            set => SetProperty(ref _bookingFrom, value);
        }

        private DateTime _bookingTo;
        public DateTime BookingTo
        {
            get => _bookingTo;
            set => SetProperty(ref _bookingTo, value);
        }

        public BookDetailsViewModel(IGraphService graphService, IConfigService configService) : base(graphService)
        {
            _graphService = graphService;
            _configService = configService;
        }

        public override void ViewAppearing()
        {
            BookingDetails = _configService.Calendar;
            BookingFrom = DateTime.Parse(BookingDetails.Start.DateTime).ToLocalTime();
            BookingTo = DateTime.Parse(BookingDetails.End.DateTime).ToLocalTime();

            foreach (AttendeeModel attendee in BookingDetails.Attendees)
            {
                if (attendee.EmailAddress.Address == BookingDetails.Organizer.EmailAddress.Address)
                {
                    attendee.Status.Response = "organizer";
                    break;
                }
            }

            base.ViewAppearing();
        }
    }
}
