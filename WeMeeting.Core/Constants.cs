﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WeMeeting.Core
{
    public static class Constants
    {
        public static string Authority => "https://login.microsoftonline.com/common";

        public static string ClientId => "640f5cc9-c63d-415e-ae58-d0cf1c6b84ec";

        public static Uri RediectUri => new Uri(string.Format("msal{0}://auth", ClientId));

        public static IList<string> Scopes => new List<string>()
        {
            "User.Read",
            "User.ReadBasic.All",
            "Calendars.Read",
            "Calendars.Read.Shared",
            "Calendars.ReadWrite",
            "Calendars.ReadWrite.Shared",
            "People.Read"
        };

        public static string JsonContentType => "application/json";

        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}
