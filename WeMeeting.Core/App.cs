﻿using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using WeMeeting.Core.Services;

namespace WeMeeting.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.LazyConstructAndRegisterSingleton<IHttpService, HttpService>();
            Mvx.LazyConstructAndRegisterSingleton<IConfigService, ConfigService>();
            Mvx.LazyConstructAndRegisterSingleton<IGraphService, GraphService>();

            RegisterNavigationServiceAppStart<ViewModels.LoginViewModel>();
        }
    }
}
