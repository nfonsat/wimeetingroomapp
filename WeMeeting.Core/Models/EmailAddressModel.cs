﻿using System;
namespace WeMeeting.Core.Models
{
    public class EmailAddressModel
    {
        public string Name { get; set; }

        public string Address { get; set; }
    }
}
