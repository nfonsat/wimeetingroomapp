﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class ScheduleInformationModel
    {
        public string AvailabilityView { get; set; }

        public ErrorModel Error { get; set; }

        public string ScheduleId { get; set; }

        public ScheduleItemModel[] ScheduleItems { get; set; }

        public WorkingHoursModel WorkingHours { get; set; }
    }
}
