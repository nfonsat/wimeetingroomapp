﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class HeaderListModel<T> : MvxObservableCollection<T> where T : class
    {
        public string Title { get; set; }

        public HeaderListModel(string title)
        {
            Title = title;
        }
    }
}
