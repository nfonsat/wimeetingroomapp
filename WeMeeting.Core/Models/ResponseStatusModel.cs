﻿using System;
namespace WeMeeting.Core.Models
{
    public class ResponseStatusModel
    {
        public string Response { get; set; }

        public string Time { get; set; }
    }
}
