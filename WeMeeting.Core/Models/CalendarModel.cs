﻿namespace WeMeeting.Core.Models
{
    public class CalendarModel
    {
        public string Id { get; set; }

        public string CreatedDateTime { get; set; }

        public string LastModifiedDateTime { get; set; }

        public string OriginalStartTimeZone { get; set; }

        public string OriginalEndTimeZone { get; set; }

        public ResponseStatusModel ResponseStatus { get; set; }

        public string Subject { get; set; }

        public string Importance { get; set; }

        public string Sensitivity { get; set; }

        public DateTimeModel Start { get; set; }

        public DateTimeModel End { get; set; }

        public LocationModel Location { get; set; }

        public LocationModel[] Locations { get; set; }

        public AttendeeModel[] Attendees { get; set; }

        public OrganizerModel Organizer { get; set; }
    }
}
