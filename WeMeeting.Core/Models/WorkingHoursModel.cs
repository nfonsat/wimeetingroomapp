﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class WorkingHoursModel
    {
        public string DaysOfWeek { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public TimeZoneModel TimeZone { get; set; }
    }
}
