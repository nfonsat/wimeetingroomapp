﻿using System;
namespace WeMeeting.Core.Models
{
    public class AttendeeModel
    {
        public string Type { get; set; }

        public ResponseStatusModel Status { get; set; }

        public EmailAddressModel EmailAddress { get; set; }
    }
}
