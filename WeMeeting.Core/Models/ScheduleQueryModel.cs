﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class ScheduleQueryModel
    {
        public string AvailabilityViewInterval { get; set; }

        public DateTimeModel EndTime { get; set; }

        public string[] Schedules { get; set; }

        public DateTimeModel StartTime { get; set; }
    }
}
