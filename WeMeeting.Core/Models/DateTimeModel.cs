﻿using System;
namespace WeMeeting.Core.Models
{
    public class DateTimeModel
    {
        public string DateTime { get; set; }

        public string TimeZone { get; set; }

        public static DateTimeModel CreateModel(DateTime dateTime, TimeZoneInfo timeZone)
        {
            return new DateTimeModel { DateTime = dateTime.ToString("s"), TimeZone = timeZone.Id };
        }
    }
}
