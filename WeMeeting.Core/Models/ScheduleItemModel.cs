﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class ScheduleItemModel
    {
        public DateTimeModel End { get; set; }

        public bool IsPrivate { get; set; }

        public string Location { get; set; }

        public DateTimeModel Start { get; set; }

        public string Status { get; set; }

        public string Subject { get; set; }
    }
}
