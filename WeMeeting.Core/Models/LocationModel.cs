﻿using System;
namespace WeMeeting.Core.Models
{
    public class LocationModel
    {
        public string UniqueId { get; set; }

        public string UniqueIdType { get; set; }

        public string DisplayName { get; set; }

        public string LocationType { get; set; }
    }
}
