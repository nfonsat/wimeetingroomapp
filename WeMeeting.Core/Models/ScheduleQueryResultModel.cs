﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class ScheduleQueryResultModel
    {
        public ScheduleInformationModel[] Value { get; set; }
    }
}
