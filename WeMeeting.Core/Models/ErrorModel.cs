﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeMeeting.Core.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public string ResponseCode { get; set; }
    }
}
