﻿using System.Threading.Tasks;
using Microsoft.Identity.Client;

namespace WeMeeting.Core.Services
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResult> AcquireTokenAsync();

        Task<AuthenticationResult> AcquireTokenSilentAsync();

        void Remove();
    }
}
