﻿using WeMeeting.Core.Models;

namespace WeMeeting.Core.Services
{
    public interface IConfigService
    {
        UserModel User { get; set; }
        CalendarModel Calendar { get; set; }
        EmailAddressModel EmailAddress { get; set; }
    }
}
