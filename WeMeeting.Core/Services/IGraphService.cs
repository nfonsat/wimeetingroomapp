﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeMeeting.Core.Models;

namespace WeMeeting.Core.Services
{
    public interface IGraphService
    {
        Task EnsureTokenIsPresentAsync();

        Task<UserModel> GetUserAsync();

        Task<CalendarModel[]> GetCalendarMeAsync(DateTime start, DateTime end);

        Task<CalendarModel[]> GetCalendarRoomAsync(string room, DateTime start, DateTime end);

        Task<EmailAddressModel[]> GetRoomsAsync();

        Task<UserModel[]> FindPeopleAsync(string search);

        Task<CalendarModel> AddEventAsync(CalendarModel calendar);

        Task<ScheduleQueryResultModel> GetFreeBusyCalendarAsync(ScheduleQueryModel schedule);

        void RemoveUser();

    }
}
