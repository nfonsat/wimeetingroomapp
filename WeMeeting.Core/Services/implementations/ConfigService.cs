﻿using WeMeeting.Core.Models;

namespace WeMeeting.Core.Services
{
    public class ConfigService : IConfigService
    {
        public UserModel User { get; set; }
        public CalendarModel Calendar { get; set; }
        public EmailAddressModel EmailAddress { get; set; }
    }
}
