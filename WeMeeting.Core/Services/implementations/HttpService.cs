﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeMeeting.Core.Extensions;

namespace WeMeeting.Core.Services
{
    public class HttpService : IHttpService
    {
        private readonly HttpClient _httpClient;

        public HttpService()
        {
            _httpClient = new HttpClient();
        }

        public Uri Resource { get; set; }

        public HttpRequestHeaders GetRequestHeaders()
        {
            return _httpClient.DefaultRequestHeaders;
        }

        public Task<T> GetAsync<T>(string resource)
        {
            return SendAsync<T>(resource, HttpMethod.Get);
        }

        public Task<T> PostAsync<T>(string resource, object data)
        {
            return SendAsync<T>(resource, HttpMethod.Post, data);
        }

        public Task<T> PostAsync<T>(string resource, Stream stream, string contentType)
        {
            return SendAsync<T>(resource, HttpMethod.Post, stream, contentType);
        }

        public async Task<T> SendAsync<T>(string resource, HttpMethod httpMethod, object data)
        {
            var str = JsonConvert.SerializeObject(data, Constants.JsonSerializerSettings);
            using (var stream = str.GetStream())
            {
                return await SendAsync<T>(resource, httpMethod,
                    stream, Constants.JsonContentType);
            }
        }

        public async Task<T> SendAsync<T>(string resource, HttpMethod httpMethod, Stream stream = null, string contentType = null)
        {
            // Create request URI.
            var requestUri = new Uri(Resource.AbsoluteUri + resource);

            // Get response.
            HttpResponseMessage response = null;
            if (httpMethod == HttpMethod.Get)
            {
                response = await _httpClient.GetAsync(requestUri);
            }
            else if (httpMethod == HttpMethod.Post)
            {
                // Create content.
                var streamContent = new StreamContent(stream);

                streamContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);

                response = await _httpClient.PostAsync(requestUri, streamContent);
            }

            if (response == null)
            {
                throw new NotImplementedException();
            }

            // Check response.
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new HttpRequestException("Unauthorized");
                }

                var errorRes = await response.Content.ReadAsStringAsync();
                throw new HttpRequestException(response.ReasonPhrase);
            }

            // Parse the response.
            var responseStr = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<T>(responseStr);
            return result;
        }
    }
}
