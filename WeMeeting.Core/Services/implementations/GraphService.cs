﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Identity.Client;
using Newtonsoft.Json;
using WeMeeting.Core.Extensions;
using WeMeeting.Core.Models;

namespace WeMeeting.Core.Services
{
    public class GraphService : IGraphService
    {
        private readonly IHttpService _httpService;

        private readonly IAuthenticationService _authenticationService;

        public string Resource => "https://graph.microsoft.com/beta/";
        
        public GraphService(IHttpService httpService,
            IAuthenticationService authenticationService)
        {
            _httpService = httpService;
            _authenticationService = authenticationService;
        }

        public async Task EnsureTokenIsPresentAsync()
        {
            var authenticationResult = await GetAuthenticationResultAsync();

            _httpService.Resource = new Uri(Resource);

            _httpService.GetRequestHeaders().Authorization = new AuthenticationHeaderValue("Bearer",
                authenticationResult.AccessToken);
        }

        public Task<UserModel> GetUserAsync()
        {
            return GetOneAsync<UserModel>("me/");
        }

        public Task<EmailAddressModel[]> GetRoomsAsync()
        {
            return GetManyAsync<EmailAddressModel>("me/findRooms");
        }

        public Task<CalendarModel[]> GetCalendarMeAsync(DateTime start, DateTime end)
        {
            var utcStart = start.ToUTCString();
            var utcEnd = end.ToUTCString();

            return GetManyAsync<CalendarModel>(string.Format("me/calendar/calendarView?startDateTime={0}&endDateTime={1}", utcStart, utcEnd));
        }

        public Task<CalendarModel[]> GetCalendarRoomAsync(string room, DateTime start, DateTime end)
        {
            var utcStart = start.ToUTCString();
            var utcEnd = end.ToUTCString();

            return GetManyAsync<CalendarModel>(string.Format("users/{0}/calendar/calendarView?startDateTime={1}&endDateTime={2}", room, utcStart, utcEnd));
        }

        public Task<ScheduleQueryResultModel> GetFreeBusyCalendarAsync(ScheduleQueryModel schedule)
        {
            return PostAsync<ScheduleQueryResultModel, ScheduleQueryModel>("me/calendar/getschedule", schedule);
        }

        public Task<UserModel[]> FindPeopleAsync(string search)
        {
            return GetManyAsync<UserModel>(String.Format("me/people/?$search={0}&$select=displayName,userPrincipalName", search));
        }

        public Task<CalendarModel> AddEventAsync(CalendarModel calendar)
        {
            return PostAsync<CalendarModel,CalendarModel>("me/events", calendar);
        }

        private async Task<T> SendAsync<T, T2>(string resource, HttpMethod httpMethod, T2 data) where T : class where T2 : class
        {
            var str = JsonConvert.SerializeObject(data, Constants.JsonSerializerSettings);
            using (var stream = str.GetStream())
            {
                return await SendAsync<T>(resource, httpMethod, stream, Constants.JsonContentType);
            }
        }

        private async Task<T> SendAsync<T>(string resource, HttpMethod httpMethod, Stream stream = null, string contentType = null) where T : class
        {
            // Try to do the call without calling ADAL. This reduces the 
            // execution time of this call drastically.
            try
            {
                if (_httpService.GetRequestHeaders().Authorization == null)
                {
                    await EnsureTokenIsPresentAsync();
                }
                return await _httpService.SendAsync<T>(resource, httpMethod, stream, contentType);
            }
            catch (HttpRequestException ex)
            {
                if (ex.Message != "Unauthorized")
                {
                    throw;
                }
            }

            // Call ADAL to validate tokens.
            try
            {
                await EnsureTokenIsPresentAsync();
                return await _httpService.SendAsync<T>(resource, httpMethod, stream, contentType);
            }
            catch (Exception e)
            {
                // Ignored.
                Console.WriteLine(e.Message);
            }
            return default(T);
        }

        private async Task<T> GetOneAsync<T>(string resource) where T : class
        {
            return await SendAsync<T>(resource, HttpMethod.Get);
        }

        private async Task<T[]> GetManyAsync<T>(string resource)
        {
            return (await SendAsync<ResponseModel<T>>(resource, HttpMethod.Get)).Value;
        }

        private async Task<T> PostAsync<T, T2>(string resource, T2 data) where T : class where T2 : class
        {
            return await SendAsync<T, T2>(resource, HttpMethod.Post, data);
        }

        private async Task<AuthenticationResult> GetAuthenticationResultAsync()
        {
            AuthenticationResult authenticationResult;
            try
            {
                // Try to get the tokens silently.
                authenticationResult = await _authenticationService.AcquireTokenSilentAsync();
                return authenticationResult;
            }
            catch (Exception e)
            {
                // Ignored.
                Console.WriteLine(e.Message);
            }

            // Prompt the user.
            authenticationResult = await _authenticationService.AcquireTokenAsync();
            return authenticationResult;
        }

        public void RemoveUser()
        {
            _authenticationService.Remove();

            _httpService.GetRequestHeaders().Authorization = null;
        }
    }
}
