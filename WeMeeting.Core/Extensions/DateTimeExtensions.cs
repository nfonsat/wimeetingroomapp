﻿using System;
namespace WeMeeting.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToUTCString(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
        }
    }
}
