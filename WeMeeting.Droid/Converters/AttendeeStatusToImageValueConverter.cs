﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace WeMeeting.Droid.Converters
{
    class AttendeeStatusToImageValueConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            switch(value.ToLower())
            {
                case "none":
                    return "res:attendee_status_none";
                case "organizer":
                    return "res:attendee_status_organizer";
                case "tentativelyaccepted":
                    return "res:attendee_status_tentativelyaccepted";
                case "accepted":
                    return "res:attendee_status_accepted";
                case "declined":
                    return "res:attendee_status_declined";
                case "notresponded":
                    return "res:attendee_status_notresponded";
                default:
                    return "";
            }
        }
    }
}