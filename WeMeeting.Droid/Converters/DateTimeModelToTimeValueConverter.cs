﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;
using WeMeeting.Core.Models;

namespace WeMeeting.Droid.Converters
{
    public class DateTimeModelToTimeValueConverter : MvxValueConverter<DateTimeModel, string>
    {
        protected override string Convert(DateTimeModel value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            return DateTime.Parse(value.DateTime).ToLocalTime().ToString("HH:mm", cultureInfo);
        }
    }
}