﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace WeMeeting.Droid.Converters
{
    public class DateTimeToDateValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            return value.ToString("ddd dd/MM/yyyy", cultureInfo);
        }
    }
}