using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.Droid.Support.V7.AppCompat;
using System.Collections.Generic;
using System.Reflection;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platform.Converters;
using WeMeeting.Droid.Converters;
using MvvmCross.Droid.Views;
using WeMeeting.Core.ViewModels;
using Android.App;
using MvvmCross.Platform.Droid.Platform;
using Android.Support.V4.App;
using WeMeeting.Droid.Views;

namespace WeMeeting.Droid
{
    public class Setup : MvxAppCompatSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            Mvx.RegisterSingleton(typeof(Core.Services.IAuthenticationService), new Services.AuthenticationService());
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IEnumerable<Assembly> AndroidViewAssemblies => 
            new List<Assembly>(base.AndroidViewAssemblies) { typeof(MvxRecyclerView).Assembly };

        protected override void FillValueConverters(IMvxValueConverterRegistry registry)
        {
            base.FillValueConverters(registry);
            registry.AddOrOverwrite("AttendeeStatusToImage", new AttendeeStatusToImageValueConverter());
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            var presenter = base.CreateViewPresenter();
            presenter.AddPresentationHintHandler<LoginPresentationHint>(hint => HandleLoginPresentationHint(hint));
            return presenter;
        }

        private bool HandleLoginPresentationHint(LoginPresentationHint hint)
        {
            Intent intent = new Intent(ApplicationContext, typeof(LoginActivity));
            intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ClearTop);
            ApplicationContext.StartActivity(intent); // Launch the mainActivity instance on top and stack after mainActivity 

            return true;
        }
    }
}
