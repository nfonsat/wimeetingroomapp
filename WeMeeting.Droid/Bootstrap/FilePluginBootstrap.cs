using MvvmCross.Platform.Plugins;

namespace WeMeeting.Droid.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}