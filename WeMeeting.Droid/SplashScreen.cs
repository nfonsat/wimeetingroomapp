using Android.App;
using Android.Content.PM;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace WeMeeting.Droid
{
    [Activity(
        Label = "WeMeeting"
        , MainLauncher = true
        , Icon = "@mipmap/icon"
        , Theme = "@style/AppTheme.Splash"
        , ScreenOrientation = ScreenOrientation.Landscape)]
    public class SplashScreen : MvxSplashScreenAppCompatActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            ZXing.Mobile.MobileBarcodeScanner.Initialize(Application);
        }
    }
}
