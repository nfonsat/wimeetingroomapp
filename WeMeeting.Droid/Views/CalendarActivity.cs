﻿using Android.App;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class CalendarActivity : BaseActivity<Core.ViewModels.CalendarViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.CalendarView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }
    }
}
