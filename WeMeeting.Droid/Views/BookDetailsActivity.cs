﻿using Android.App;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class BookActivity : BaseActivity<Core.ViewModels.BookDetailsViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.BookDetailsView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }
    }
}
