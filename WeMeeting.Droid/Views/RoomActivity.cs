﻿using Android.App;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class RoomActivity : BaseActivity<Core.ViewModels.RoomViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.RoomView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }
    }
}
