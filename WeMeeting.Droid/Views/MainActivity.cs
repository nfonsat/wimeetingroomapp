﻿using Android.App;
using Android.Content;
using Microsoft.Identity.Client;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class MainActivity : BaseActivity<Core.ViewModels.MainViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.MainView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }
    }
}
