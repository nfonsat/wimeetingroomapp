﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Java.Util;
using WeMeeting.Core.ViewModels;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class BookRoomActivity : BaseActivity<BookRoomViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.BookRoomView;
        private TextView _datePickerFrom;
        private TextView _datePickerTo;
        private TextView _timePickerFrom;
        private TextView _timePickerTo;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _datePickerFrom = FindViewById<TextView>(Resource.Id.datePickerFrom);
            _datePickerTo = FindViewById<TextView>(Resource.Id.datePickerTo);
            _timePickerFrom = FindViewById<TextView>(Resource.Id.timePickerFrom);
            _timePickerTo = FindViewById<TextView>(Resource.Id.timePickerTo);

            _datePickerFrom.Click += delegate (object sender, EventArgs e)
            {
                Calendar calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);

                new DatePickerDialog(this,
                    delegate (object s, DatePickerDialog.DateSetEventArgs date)
                    {
                        ViewModel.BookFrom = new DateTime(date.Year, date.Month + 1, date.DayOfMonth,
                                                          ViewModel.BookFrom.Hour, ViewModel.BookFrom.Minute, 0);
                        ViewModel.BookTo = ViewModel.BookFrom.AddMinutes(30);
                    },
                    calendar.Get(CalendarField.Year),
                    calendar.Get(CalendarField.Month),
                    calendar.Get(CalendarField.DayOfMonth)).Show();
            };

            _datePickerTo.Click += delegate (object sender, EventArgs e)
            {
                Calendar calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);

                new DatePickerDialog(this,
                    delegate (object s, DatePickerDialog.DateSetEventArgs date)
                    {
                        ViewModel.BookTo = new DateTime(date.Year, date.Month + 1, date.DayOfMonth,
                                                        ViewModel.BookTo.Hour, ViewModel.BookTo.Minute, 0);
                    },
                    calendar.Get(CalendarField.Year),
                    calendar.Get(CalendarField.Month),
                    calendar.Get(CalendarField.DayOfMonth)).Show();
            };

            _timePickerFrom.Click += delegate (object sender, EventArgs e)
            {
                Calendar calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);

                new TimePickerDialog(this,
                    delegate (object s, TimePickerDialog.TimeSetEventArgs time)
                    {
                        ViewModel.BookFrom = new DateTime(ViewModel.BookFrom.Year, ViewModel.BookFrom.Month, ViewModel.BookFrom.Day,
                                                          time.HourOfDay, time.Minute, 0);
                        ViewModel.BookTo = ViewModel.BookFrom.AddMinutes(30);
                    },
                    calendar.Get(CalendarField.HourOfDay),
                    calendar.Get(CalendarField.Minute),
                    true).Show();
            };

            _timePickerTo.Click += delegate (object sender, EventArgs e)
            {
                Calendar calendar = Calendar.GetInstance(Java.Util.TimeZone.Default);

                new TimePickerDialog(this,
                    delegate (object s, TimePickerDialog.TimeSetEventArgs time)
                    {
                        ViewModel.BookTo = new DateTime(ViewModel.BookTo.Year, ViewModel.BookTo.Month, ViewModel.BookTo.Day,
                                                        time.HourOfDay, time.Minute, 0);
                    },
                    calendar.Get(CalendarField.HourOfDay),
                    calendar.Get(CalendarField.Minute),
                    true).Show();
            };
        }
    }

}
