﻿using Android.App;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "WeMeeting", Theme = "@style/AppActionBar")]
    public class BookDetailsActivity : BaseActivity<Core.ViewModels.BookViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.BookView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }
    }
}
