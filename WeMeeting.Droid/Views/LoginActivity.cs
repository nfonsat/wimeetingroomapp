﻿using Android.App;
using Android.Content;
using Microsoft.Identity.Client;

namespace WeMeeting.Droid.Views
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : BaseActivity<Core.ViewModels.LoginViewModel>
    {
        protected override int ResourceLayout => Resource.Layout.LoginView;

        protected override void OnResume()
        {
            ViewModel.OnResume();
            base.OnResume();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationContinuationHelper.SetAuthenticationContinuationEventArgs(requestCode, resultCode, data);
        }
    }
}
