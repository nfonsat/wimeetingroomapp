﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace WeMeeting.Droid.Views
{
    public abstract class BaseActivity : MvxAppCompatActivity
    {
        protected abstract int ResourceLayout { get; }
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Landscape;
        }

        protected override void OnViewModelSet()
        {
            SetContentView(ResourceLayout);
            base.OnViewModelSet();
        }
    }

    public abstract class BaseActivity<TViewModel> : BaseActivity
        where TViewModel : class, MvvmCross.Core.ViewModels.IMvxViewModel
    {
        public new TViewModel ViewModel
        {
            get { return (TViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
    }
}
